from flask import Flask, render_template, request, redirect, url_for
import sqlite3 as lite 
import sys
import requests
from peewee import *
import timeit
from itsdangerous import URLSafeTimedSerializer, SignatureExpired, URLSafeSerializer
import models as dbHandler

global s
s = URLSafeSerializer('znf04')

DATABASE = 'Tugas.db'
database = SqliteDatabase(DATABASE) #for log-only sqlite 

class BaseModel(Model):
    class Meta:
        database = database

class Tbuser(BaseModel):
    NIK = CharField(unique=True)
    Nama = TextField()
    level = TextField()
    Email = TextField()
    Pass = TextField()

def create_tables():
    with database:
        database.create_tables([Tbuser])

app = Flask(__name__)

@app.route("/")
def Index():
    return render_template('index.html')

@app.route("/login")
def Login():
    return render_template('login.html')

@app.route("/actionlogin", methods=['GET','POST'])
def actionLogin():
    if request.method=='POST':
        username = request.form['email']
        password = request.form['pass']
        users = dbHandler.retrieveUsers(username, password)
        #return render_template('dashboard.html', users=users)
        while(users) > 0:
            return render_template('dashboard.html',users=users)
        else:
            return render_template('login.html')
                

@app.route("/isiregister")
def isiRegister():
    sesuatu = "Isi Data Anda"
    return render_template('register.html',pesan=sesuatu)

@app.route("/simpandata", methods=['GET', 'POST'])
def simpanData():
    
    nik_f = request.form['nik']
    nama_f = request.form['nama']
    level_f = request.form['level']
    email_f = request.form['email']
    pass_f = request.form['pass']

    car_entry = Tbuser.create(
        NIK = nik_f,
        Nama = nama_f,
        level = level_f,
        Email = email_f,
        Pass = pass_f
    )
    return redirect(url_for('Login'))

if __name__ == "__main__": 
    create_tables()
    app.run(

        )